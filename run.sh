sudo apt update
sudo apt install -y software-properties-common
# sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y

ansible-galaxy install -r requirements.yml --roles-path galaxy_roles --force

ansible-playbook playbook.yml --ask-become-pass

