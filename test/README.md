# Install vagrant:
    sudo apt install vagrant virtualbox
# Execute vagrant
    vagrant up --provision

# Destroy vagrant machines
    vagrant destroy -f